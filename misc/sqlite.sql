-- province
CREATE TABLE `province` (
    `code` VARCHAR(255) PRIMARY KEY, 
    `name` VARCHAR(255)
);

-- city
CREATE TABLE `city` (
    `code` VARCHAR(255) PRIMARY KEY, 
    `name` VARCHAR(255), 
    `provinceCode` VARCHAR(255) REFERENCES `province` (`code`) ON DELETE SET NULL ON UPDATE CASCADE
);

-- area
CREATE TABLE `area` (
    `code` VARCHAR(255) PRIMARY KEY, 
    `name` VARCHAR(255), 
    `cityCode` VARCHAR(255) REFERENCES `city` (`code`) ON DELETE SET NULL ON UPDATE CASCADE, 
    `provinceCode` VARCHAR(255) REFERENCES `province` (`code`) ON DELETE SET NULL ON UPDATE CASCADE
);

-- street
CREATE TABLE `street` (
    `code` VARCHAR(255) PRIMARY KEY, 
    `name` VARCHAR(255), 
    `areaCode` VARCHAR(255) REFERENCES `area` (`code`) ON DELETE SET NULL ON UPDATE CASCADE, 
    `provinceCode` VARCHAR(255) REFERENCES `province` (`code`) ON DELETE SET NULL ON UPDATE CASCADE, 
    `cityCode` VARCHAR(255) REFERENCES `city` (`code`) ON DELETE SET NULL ON UPDATE CASCADE
);

-- village
CREATE TABLE `village` (
    `code` VARCHAR(255) PRIMARY KEY, 
    `name` VARCHAR(255), 
    `streetCode` VARCHAR(255) REFERENCES `street` (`code`) ON DELETE SET NULL ON UPDATE CASCADE, 
    `provinceCode` VARCHAR(255) REFERENCES `province` (`code`) ON DELETE SET NULL ON UPDATE CASCADE, 
    `cityCode` VARCHAR(255) REFERENCES `city` (`code`) ON DELETE SET NULL ON UPDATE CASCADE, 
    `areaCode` VARCHAR(255) REFERENCES `area` (`code`) ON DELETE SET NULL ON UPDATE CASCADE
);

-- adcode
DROP TABLE IF EXISTS "adcode";
CREATE TABLE "adcode" (
  "code" TEXT NOT NULL,
  "name" TEXT NOT NULL,
  "level" INTEGER NOT NULL,
  "parent_code" TEXT,
  PRIMARY KEY ("code")
);

CREATE INDEX "parent_code_idx" ON "adcode" ("parent_code");
CREATE INDEX "name_idx" ON "adcode" ("name");