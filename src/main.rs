#![allow(dead_code)]

use std::sync::Arc;

use clap::StructOpt;
use rustyline::{error::ReadlineError, Editor};
use store::AdcodeStore;

mod migrate;
mod store;
mod types;

#[derive(Debug, clap::Parser)]
struct Args {
    #[clap(long)]
    migrate: bool,
}

type Handler = fn(&AdcodeStore, &[&str]);

#[tokio::main]
async fn main() {
    let args = Args::parse();
    if args.migrate {
        migrate::MigrateRunner::new("./data.sqlite")
            .await
            .run()
            .await;
        return;
    }

    let store = Arc::new(AdcodeStore::new("./data.sqlite").await);

    // 接收用户输入，返回是否退出
    cmd_loop(|line: String| -> bool {
        if line.is_empty() {
            return false;
        }

        let store = store.clone();
        let handler = tokio::spawn(async move {
            let mut inputs = line.split(' ');
            let cmd = inputs.next().unwrap().trim();
            let inputs = inputs.map(|s| s.trim()).collect::<Vec<&str>>();
            match cmd {
                "parents" => store.parents(&inputs).await,
                "children" => store.children(&inputs).await,
                "search" => store.search(&inputs).await,
                "exit" => return true,
                _ => println!("unsupported cmd"),
            }
            false
        });
        futures::executor::block_on(handler).unwrap()
    });
}

fn cmd_loop(f: impl Fn(String) -> bool) {
    // 从标准输入读取
    let mut rl = Editor::<()>::new();
    loop {
        let readline = rl.readline(">> ");
        match readline {
            Ok(line) => {
                if f(line.clone()) {
                    return;
                }
                rl.add_history_entry(line.as_str());
            }
            Err(ReadlineError::Interrupted) => {
                println!("CTRL-C");
                break;
            }
            Err(ReadlineError::Eof) => {
                println!("CTRL-D");
                break;
            }
            Err(err) => {
                println!("Error: {:?}", err);
                break;
            }
        }
    }
}
