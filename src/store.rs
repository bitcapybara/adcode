use std::{collections::HashMap, sync::Arc, vec};

use futures::TryStreamExt;
use sqlx::{Row, Sqlite, SqlitePool};
use tokio::sync::Mutex;

use crate::types::Adcode;

pub(crate) struct AdcodeStore {
    // 前四级的数据
    code_idx: Mutex<HashMap<String, Arc<Adcode>>>,
    parent_code_idx: Mutex<HashMap<Option<String>, Vec<Arc<Adcode>>>>,
    // 数据库
    pool: SqlitePool,
}

impl AdcodeStore {
    // 初始化，加载数据
    pub(crate) async fn new(data_path: &str) -> Self {
        let s = Self {
            code_idx: Mutex::new(HashMap::with_capacity(44719)),
            parent_code_idx: Mutex::new(HashMap::with_capacity(44719)),
            pool: sqlx::SqlitePool::connect(data_path).await.unwrap(),
        };

        // 前四级总数
        println!("start initial data...");
        let row =
            sqlx::query::<Sqlite>(r#"SELECT COUNT(code) as count FROM adcode WHERE "level"<5;"#)
                .fetch_one(&s.pool)
                .await
                .unwrap();
        let count: i64 = row.try_get("count").unwrap();
        let mut progress_c = 0;

        const SELECT_BATCH_SIZE: i64 = 1000;
        for offset in (0..count).step_by(SELECT_BATCH_SIZE as usize) {
            let mut rows = sqlx::query::<Sqlite>(
                r#"SELECT "code", "name", "level", "parent_code" FROM adcode WHERE "level"<5 ORDER BY "code" LIMIT ? OFFSET ? ;"#,
            )
            .bind(SELECT_BATCH_SIZE)
            .bind(offset)
            .fetch(&s.pool);

            let mut code_idx = s.code_idx.lock().await;
            let mut parent_idx = s.parent_code_idx.lock().await;
            while let Some(row) = rows.try_next().await.unwrap() {
                let adcode: Arc<Adcode> = Arc::new(row.into());
                // 放入内存索引
                code_idx.insert(adcode.code.clone(), adcode.clone());
                match parent_idx.get_mut(&adcode.parent_code) {
                    Some(ads) => {
                        ads.push(adcode.clone());
                    }
                    None => {
                        parent_idx.insert(adcode.parent_code.clone(), vec![adcode.clone()]);
                    }
                }

                progress_c += 1;
                print!("\r{}%", progress_c * 100 / count);
            }
        }

        s
    }

    // 按名称查询节点的所有父级，按照根到自己的顺序打印
    // 传入多个时，分批打印
    pub(crate) async fn parents(&self, inputs: &[&str]) {
        // 先访问数据库，找到目标节点
        let adcodes = self.by_name(inputs).await;

        // 每个节点，查找其祖先
        for adcode in adcodes {
            let parents = self.get_parents(&adcode.name, adcode.parent_code).await;
            println!("{}", parents.join(" "));
        }
    }

    // 按名称查询节点的所有下一级成员
    // 传入多个时，分批打印
    pub(crate) async fn children(&self, inputs: &[&str]) {
        let adcodes = self.by_name(inputs).await;

        for adcode in adcodes {
            let parents = self.get_parents(&adcode.name, adcode.parent_code).await;

            println!("-----{}-----", parents.join(" "));
            match adcode.level {
                l if l < 4 => {
                    // 内存查找
                    println!("{}", self.get_children(&adcode.code).await.join(" "));
                }
                4 => {
                    // 查数据库
                    let mut rows = sqlx::query::<Sqlite>(
                        r#"SELECT "name" FROM adcode WHERE "level"=5 AND parent_code = ? ;"#,
                    )
                    .bind(adcode.code)
                    .fetch(&self.pool);
                    let mut res: Vec<String> = vec![];
                    while let Some(row) = rows.try_next().await.unwrap() {
                        res.push(row.try_get("name").unwrap());
                    }
                    println!("{}", res.join(" "));
                }
                _ => {}
            }
        }
    }

    // 按名称模糊搜索节点
    pub(crate) async fn search(&self, inputs: &[&str]) {
        let mut names: Vec<String> = vec![];
        for name in inputs {
            let mut rows =
                sqlx::query::<Sqlite>(r#"SELECT DISTINCT(name) FROM adcode where "name" LIKE ?"#)
                    .bind(format!("%{}%", name))
                    .fetch(&self.pool);

            while let Some(row) = rows.try_next().await.unwrap() {
                names.push(row.try_get("name").unwrap());
            }
        }

        println!("{}", names.join(" "));
    }

    async fn get_parents(&self, name: &str, parent_code: Option<String>) -> Vec<String> {
        let mut parents = Vec::with_capacity(5);
        // 先把自己加进来
        parents.push(name.into());

        let mut parent_code = parent_code;
        while let Some(p) = parent_code {
            let idx = self.code_idx.lock().await;
            let ad = idx.get(&p).unwrap();
            parents.push(ad.name.clone());
            parent_code = ad.parent_code.clone();
        }

        parents.reverse();
        parents
    }

    async fn get_children(&self, code: &str) -> Vec<String> {
        let idx = self.parent_code_idx.lock().await;
        idx.get(&Some(code.to_string()))
            .unwrap()
            .iter()
            .map(|s| s.name.clone())
            .collect::<Vec<String>>()
    }

    // 按照名称精确搜索
    async fn by_name(&self, names: &[&str]) -> Vec<Adcode> {
        let mut res = vec![];
        for name in names {
            let mut rows = sqlx::query::<Sqlite>(r#"SELECT * FROM adcode where "name"=?"#)
                .bind(name)
                .fetch(&self.pool);

            while let Some(row) = rows.try_next().await.unwrap() {
                res.push(row.into());
            }
        }

        res
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    // cargo test store::tests::parents -- --nocapture
    #[tokio::test]
    async fn parents() {
        let store = AdcodeStore::new("./data.sqlite").await;
        store.parents(&["顺义区", "朝阳区"]).await;
    }

    // cargo test store::tests::children -- --nocapture
    #[tokio::test]
    async fn children() {
        let store = AdcodeStore::new("./data.sqlite").await;
        store.children(&["三交镇", "岚县"]).await;
    }

    // cargo test store::tests::search -- --nocapture
    #[tokio::test]
    async fn search() {
        let store = AdcodeStore::new("./data.sqlite").await;
        store.search(&["辖"]).await;
    }
}
