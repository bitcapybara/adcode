use sqlx::{sqlite::SqliteRow, Row};

#[derive(Debug, Clone)]
pub(crate) struct Adcode {
    pub(crate) code: String,
    pub(crate) name: String,
    pub(crate) level: i16,
    pub(crate) parent_code: Option<String>,
}

impl std::convert::From<SqliteRow> for Adcode {
    fn from(row: SqliteRow) -> Self {
        Self {
            code: row.try_get("code").unwrap(),
            name: row.try_get("name").unwrap(),
            level: row.try_get("level").unwrap(),
            parent_code: row.try_get("parent_code").unwrap(),
        }
    }
}
