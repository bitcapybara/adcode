use std::sync::Arc;

use futures::{future, TryStreamExt};
use sqlx::{Row, Sqlite, SqlitePool};
use tokio::time;

use crate::types::Adcode;

const SELECT_BATCH_SIZE: usize = 1000;
const INSERT_BATCH_SIZE: usize = 100;

pub(crate) struct MigrateRunner {
    pool: SqlitePool,
}

impl MigrateRunner {
    pub(crate) async fn new(data_path: &str) -> Self {
        Self {
            pool: sqlx::SqlitePool::connect(data_path).await.unwrap(),
        }
    }

    // 运行
    pub(crate) async fn run(self) {
        let start = time::Instant::now();

        let s = Arc::new(self);

        let s1 = s.clone();
        let h1 = tokio::spawn(async move {
            s1.clone().province().await;
            s1.clone().city().await;
            s1.clone().area().await;
            s1.clone().street().await;
        });

        let s2 = s.clone();
        let h2 = tokio::spawn(async move {
            s2.village().await;
        });

        future::join_all(vec![h1, h2]).await;

        println!("total spent {:?}", start.elapsed());
    }

    // 迁移省级数据
    async fn province(self: Arc<Self>) {
        let start = time::Instant::now();
        self.migrate("province", None, 1).await;
        println!("province spent {:?}", start.elapsed());
    }

    // 迁移地级数据
    async fn city(self: Arc<Self>) {
        let start = time::Instant::now();
        self.migrate("city", Some("provinceCode"), 2).await;
        println!("city spent {:?}", start.elapsed());
    }

    // 迁移县级数据
    async fn area(self: Arc<Self>) {
        let start = time::Instant::now();
        self.migrate("area", Some("cityCode"), 3).await;
        println!("area spent {:?}", start.elapsed());
    }

    // 迁移乡级数据
    async fn street(self: Arc<Self>) {
        let start = time::Instant::now();
        self.migrate("street", Some("areaCode"), 4).await;
        println!("street spent {:?}", start.elapsed());
    }

    // 迁移村级数据
    async fn village(self: Arc<Self>) {
        let start = time::Instant::now();
        self.migrate("village", Some("streetCode"), 5).await;
        println!("village spent {:?}", start.elapsed());
    }

    // 按数据量，将数据分批迁移，每批开一个协程
    async fn migrate(self: Arc<Self>, table: &str, parent_col: Option<&str>, level: i16) {
        // 获取总数
        let row = sqlx::query::<Sqlite>(&format!("SELECT COUNT(code) as count from {};", table))
            .fetch_one(&self.pool)
            .await
            .unwrap();
        let count: i64 = row.try_get("count").unwrap();
        let limit = (count as usize).min(SELECT_BATCH_SIZE);

        let mut handlers = Vec::with_capacity(count as usize / SELECT_BATCH_SIZE + 1);
        for offset in (0..count as usize).step_by(SELECT_BATCH_SIZE) {
            let s = self.clone();
            let table = String::from(table);
            let parent_col = parent_col.map(String::from);
            let handler = tokio::spawn(async move {
                // 获取所有地级数据
                let datas = s
                    .select(&table, parent_col.as_deref(), level, offset, limit)
                    .await;
                if datas.is_empty() {
                    return;
                }
                // 批量插入
                s.insert(datas).await;
            });
            handlers.push(handler);
        }

        future::join_all(handlers).await;
    }

    // 获取数据
    async fn select(
        &self,
        table: &str,
        parent_col: Option<&str>,
        level: i16,
        offset: usize,
        limit: usize,
    ) -> Vec<Adcode> {
        let mut res = Vec::with_capacity(limit);
        let sql = match parent_col.as_ref() {
            Some(parent) => {
                format!(
                    "SELECT code, name, {} FROM {} ORDER BY code LIMIT {} OFFSET {};",
                    parent, table, limit, offset
                )
            }
            None => {
                format!(
                    "SELECT code, name FROM {} ORDER BY code LIMIT {} OFFSET {};",
                    table, limit, offset
                )
            }
        };
        let mut rows = sqlx::query::<Sqlite>(&sql).fetch(&self.pool);
        while let Some(row) = rows.try_next().await.unwrap() {
            let mut parent_code = None;
            if let Some(parent) = parent_col {
                parent_code.replace(row.try_get(parent).unwrap());
            }

            res.push(Adcode {
                code: row.try_get("code").unwrap(),
                name: row.try_get("name").unwrap(),
                level,
                parent_code,
            })
        }

        res
    }

    // 把传入的数据，按照 INSERT_BATCH_SIZE 分组，批量插入
    async fn insert(&self, datas: Vec<Adcode>) {
        let length = datas.len();

        let mut values = Vec::with_capacity(INSERT_BATCH_SIZE);
        let mut batch = 0;

        for (i, data) in datas.into_iter().enumerate() {
            batch += 1;
            values.push(data);
            if batch < INSERT_BATCH_SIZE && i != length - 1 {
                continue;
            }
            let s = (0..batch)
                .map(|i| {
                    format!(
                        "(${}, ${}, ${}, ${})",
                        4 * i + 1,
                        4 * i + 2,
                        4 * i + 3,
                        4 * i + 4
                    )
                })
                .collect::<Vec<String>>()
                .join(", ");
            let sql = format!(
                r#"INSERT INTO adcode ( code, "name", "level", parent_code ) VALUES {};"#,
                s
            );
            let mut query = sqlx::query::<Sqlite>(&sql);
            for v in values.iter() {
                query = query
                    .bind(&v.code)
                    .bind(&v.name)
                    .bind(&v.level)
                    .bind(&v.parent_code);
            }

            query.execute(&self.pool).await.unwrap();

            batch = 0;
            values.clear();
        }
    }
}

#[cfg(test)]
mod tests {
    use super::MigrateRunner;
    use std::sync::Arc;

    #[tokio::test]
    async fn province() {
        let runner = MigrateRunner::new("./data.sqlite").await;
        Arc::new(runner).province().await;
    }

    #[tokio::test]
    async fn city() {
        let runner = MigrateRunner::new("./data.sqlite").await;
        Arc::new(runner).city().await;
    }

    #[tokio::test]
    async fn area() {
        let runner = MigrateRunner::new("./data.sqlite").await;
        Arc::new(runner).area().await;
    }

    #[tokio::test]
    async fn street() {
        let runner = MigrateRunner::new("./data.sqlite").await;
        Arc::new(runner).street().await;
    }

    #[tokio::test]
    async fn village() {
        let runner = MigrateRunner::new("./data.sqlite").await;
        Arc::new(runner).village().await;
    }
}
